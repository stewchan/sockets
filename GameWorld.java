import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Map;
import java.util.HashMap;

/**
 * The Game world.
 * This is also the main game center and controller.
 * 
 * @author Chan 
 * @version November 2018
 */
public class GameWorld extends World
{
    public GameClient client;
    Player localPlayer;
    
    // All the remote player objects
    private Map<String, Player> remotePlayers = new HashMap<String, Player>(); 
       
    public GameWorld(Player player, GameClient client)
    {    
        super(375, 680, 1);
        this.client = client;
        localPlayer = player;
        addObject(player, getWidth()/2, getHeight()/2);
    }
    
    /**
     * Main game loop
     */
    public void act()
    {
        try {
            client.update(localPlayer.getNetworkInfo());
        } catch(Exception e) {
            System.out.println(e + "Error in game world.");
        }
    }
    
    /**
     * Take player network info from server and display remote players on screen
     * This method is meant to be called by the network client to display remote players
     */
    public void update(Map<String, PlayerData> remotePlayersInfo)
    {
        remotePlayersInfo.remove(localPlayer.getName());  
        
        for(String name : remotePlayersInfo.keySet()) {
            Player player;
            PlayerData netInfo = remotePlayersInfo.get(name);
            if(remotePlayers.containsKey(name)) {
                // Update playerNetWork info in the player object
                player = remotePlayers.get(name);
                player.setNetworkInfo(netInfo);
            } else {
                // Create a new player and put in map of remote players
                player = new Player(netInfo, false);
                addObject(player, getWidth()/2, getHeight()/2);
                player.update();
                remotePlayers.put(name, player);
            }
            player.updateFromNetInfo();

        }
    }
    
}

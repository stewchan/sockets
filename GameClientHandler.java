import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.Map;
import java.util.HashMap;

/**
 * GameClientHandler is a server that allows multiple clients to run.
 * 
 * @author Chan 
 * @version November 2018
 */
public class GameClientHandler extends Thread
{
    private Socket socket;
    private ServerData data;
    
    public GameClientHandler(Socket socket, ServerData data)
    {
        this.socket = socket;
        this.data = data;
    }
    
    public void run()
    {
        try{
            ObjectOutputStream toClient = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            ObjectInputStream fromClient = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
            while(true)
            {
                toClient.flush();
                PlayerData playerInfo = (PlayerData) fromClient.readObject();
                process(playerInfo);
                playerInfo = null;
                toClient.writeUnshared(data.getPlayersMap());
            }
        } catch(Exception e) {
            System.out.println("Error in GameClientHandler");
        }
    }
    
    
    /**
     * Process the Player info object coming in.
     * TODO: Make every player have a unique network ID and put players in an arraylist
     */
    public void process(PlayerData playerInfo)
    {   
        String name = playerInfo.getName();
        if(data.getPlayersMap().containsKey(name)) {
            // update info for an existing player
            data.getPlayersMap().replace(name, playerInfo);
        } else {
            // Add new player to game data
            data.getPlayersMap().put(name, playerInfo);
        }
    }
    
}

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

/**
 * Holds all the shared network data on the server.
 * 
 * @author Chan 
 * @version November 2018
 */
public class ServerData implements Serializable
{
    private Map<String, PlayerData> remotePlayers;

    public ServerData()
    {
        remotePlayers = new HashMap<String,PlayerData>();
    }
    
    public Map<String, PlayerData> getPlayersMap()
    {
        return remotePlayers;
    }
    
    public String toString()
    {
        String str = "";
        for(String name : remotePlayers.keySet()) {
            PlayerData info = remotePlayers.get(name);
            str += name + " x:" + info.getX() + " y:" + info.getY() + "\n";
        }
        return str;
    }
}

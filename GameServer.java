import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.ArrayList;

/**
 * A class for testing network sockets.
 * 
 * @author Chan
 * @version November 2018
 */
public class GameServer implements Runnable
{   
    // The main shared data store ONLY ONE INSTANCE
    private ServerData data;
    private List<GameClientHandler> clientHandlers = new ArrayList<GameClientHandler>();
    private ExecutorService executor;
    
    public void run()
    {
        ServerSocket servSocket = null;
        boolean successful = false;
        int port = 5444;
        
        try{
            servSocket = new ServerSocket(port);
            successful = true;
        } catch(IOException e) {
            System.err.println(port + " is busy.  Use a different port.");
        }
        
        if(successful){
            if (data == null) {
                data = new ServerData();
            }
            
            try{
                while(true) {
                    GameClientHandler handler = new GameClientHandler(servSocket.accept(), data);
                    clientHandlers.add(handler);
                    handler.start();
                }
            } catch(Exception e) {
                try{
                    servSocket.close();
                    executor.shutdown();
                    if (!executor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                        executor.shutdownNow();
                    } 
                } catch(InterruptedException ie) {
                    executor.shutdownNow();
                } catch(IOException ioe) {
                    System.err.println(e);
                }
            }
        }
    }
    
    public static void startServer()
    {
        new Thread(new GameServer()).start();
    }    
}

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Timer;
import java.util.TimerTask;

/**
 * A Label class that allows you to display a textual value on screen.
 * 
 * The Label is an actor, so you will need to create it, and then add it to the world
 * in Greenfoot.  If you keep a reference to the Label then you can change the text it
 * displays.  
 *
 * @author Amjad Altadmri, Stewart Chan
 * @version November 2018
 */
public class Label extends Actor
{
    protected String value;
    protected int fontSize;
    private Color lineColor = Color.BLACK;
    private Color fillColor = Color.BLACK;
    
    private static final Color transparent = new Color(0,0,0,0);
    
    
    /**
     * Create a new label, initialise it with the int value to be shown and the font size 
     */
    public Label(int value, int fontSize)
    {
        this(Integer.toString(value), fontSize);
    }
    
    /**
     * Create a new label, initialise it with the needed text and the font size 
     */
    public Label(String value, int fontSize)
    {
        this.value = value;
        this.fontSize = fontSize;
        updateImage();
    }

    /**
     * Sets the value  as text
     * 
     * @param value the text to be show
     */
    public void setValue(String value)
    {
        this.value = value;
        updateImage();
    }
    
    /**
     * Sets the value as integer
     * 
     * @param value the value to be show
     */
    public void setValue(int value)
    {
        this.value = Integer.toString(value);
        updateImage();
    }
    
    /**
     * Sets the value with delay
     */
    public void setValueWithDelay(String value, int delay)
    {
        setValue(value);
        Greenfoot.delay(delay);
    }
    
    /**
     * Show a fading connection animation
     */
    public void setValueWithAnimation(String value)
    {
        setValue(value);
        TimerTask task = new TimerTask() {
            int trans = 100;
            int amt = -1;
            public void run() {
                GreenfootImage image = getImage();
                image.setTransparency(trans);
                trans += amt;
                if(trans >= 100)
                    amt = -1;
                if(trans <= 30)
                    amt = 1;
                updateImage();
            }
        };
        
        Timer timer = new Timer("AnimTimer");
        long delay = 10L;
        long period = 10L;
        timer.scheduleAtFixedRate(task, delay, period);
    }
    
    /**
     * Sets the line color of the text
     * 
     * @param lineColor the line color of the text
     */
    public void setLineColor(Color lineColor)
    {
        this.lineColor = lineColor;
        updateImage();
    }
    
    /**
     * Sets the fill color of the text
     * 
     * @param fillColor the fill color of the text
     */
    public void setFillColor(Color fillColor)
    {
        this.fillColor = fillColor;
        updateImage();
    }
    

    /**
     * Update the image on screen to show the current value.
     */
    private void updateImage()
    {
        setImage(getCustomImage());
    }
    
    /**
     * Gets the image of the string or number
     */
    public GreenfootImage getCustomImage()
    {
        return new GreenfootImage(value, fontSize, fillColor, transparent, lineColor);
    }
}
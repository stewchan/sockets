import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The in-game player actor
 * 
 * @author Chan 
 * @version November 2018
 */
public class Player extends Actor
{
    private boolean localPlayer = false;
    private int speed = 3;
    private PlayerData PlayerData;  // Network info object
    private String name;  // This player's name
        
    public Player(PlayerData PlayerData, boolean localPlayer)
    {
        this.PlayerData = PlayerData;
        this.localPlayer = localPlayer;
        
        name = PlayerData.getName();
        Color color = Color.BLACK;
        Color transparent = new Color(0,0,0,0);
        
        // Local player is always blue, everyone else is another color
        if(localPlayer) {
            color = Color.BLUE;
        } else {
            color = Color.RED;
        }
        
        setImage(new GreenfootImage(name, 30, color, transparent, Color.BLACK));
    }
        
    public void act() 
    {
        // If this object is the main player, they can move it
        if (localPlayer) {
            controlPlayer();
        }
    }    
    
    private void controlPlayer()
    {
        if(Greenfoot.isKeyDown("up")) {  setLocation(getX(), getY() - speed); }
        if(Greenfoot.isKeyDown("down")) {  setLocation(getX(), getY() + speed); }
        if(Greenfoot.isKeyDown("left")) {  setLocation(getX() - speed, getY()); }
        if(Greenfoot.isKeyDown("right")) { setLocation(getX() + speed, getY()); }
        updateToNetInfo();
    }
    
    private void updateToNetInfo()
    {
        PlayerData.setX(getX());
        PlayerData.setY(getY());
    }

    /**
     * Methods for remote player control
     */
    public void updateFromNetInfo()
    {
        setLocation(PlayerData.getX(), PlayerData.getY());
    }
    
    public void setNetworkInfo(PlayerData netInfo)
    {
        this.PlayerData = netInfo;
        name = netInfo.getName();
    }
    
    public PlayerData getNetworkInfo()
    {
        return PlayerData;
    }
    
    /**
     * Update player's position based on network info
     */
    public void update()
    {
        int x = PlayerData.getX();
        int y = PlayerData.getY();
        setLocation(x, y);
    }
    
    public String getName()
    {
        return name;
    }
    
    public String toString() {
        return "Player: " + getName();
    }
}

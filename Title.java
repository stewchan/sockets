import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Title scene.
 * 
 * @author Chan
 * @version November 2018
 */
public class Title extends World
{    
    private GameClient client;  // The game network client
    
    private Label messageLabel;
    private Label ipLabel;
    private Button serverButton;
    private Button ipButton;
    private Button connectButton;
    private Label hostIpLabel;
    private String hostIP = "";
    
    private String title = "Welcome!";
    private boolean connected = false;

    private static final int SCREEN_WIDTH = 375;
    private static final int SCREEN_HEIGHT = 680;
    
    public Title()
    {    
        super(SCREEN_WIDTH, SCREEN_HEIGHT, 1);
        client = new GameClient(); 
        setupUI();
    }

    private void setupUI()
    {
        messageLabel = new Label(title, 45);
        addObject(messageLabel, getWidth()/2, getHeight() * 6/24); 
        
        connectButton = new Button("Join Game", 45);
        addObject(connectButton, getWidth()/2, getHeight() * 12/24);
        
        serverButton = new Button("Host Game", 23);
        addObject(serverButton, getWidth()/2, getHeight() * 16/24);
        
        ipButton = new Button("Enter Host IP", 22);
        addObject(ipButton, getWidth()/2, getHeight() * 18/24);
        
        hostIpLabel = new Label("Host IP:  " + client.getHostIP(), 22);
        addObject(hostIpLabel, getWidth()/2, getHeight() * 20/24);
        updateHostIpLabel();
        
        ipLabel = new Label("Your IP:  " + client.getLocalIP(), 22);
        addObject(ipLabel, getWidth()/2, getHeight() * 21/24);
    }
    
    /**
     * Try and connect to a server
     */
    public void attemptConnection()
    {
        int delay = 35;
        if (!connected) {
            messageLabel.setValueWithDelay("Connecting...", delay);
            if(client.connect(hostIP)) {
                connected = true;
                messageLabel.setValueWithDelay("Connected!", delay);
                String name = Greenfoot.ask("Enter Your Player's Name:");
                PlayerData PlayerData = new PlayerData(name);
                Player player = new Player(PlayerData, true);  // Create the local player
                GameWorld game = new GameWorld(player, client);  // Create the game world
                client.initialize(PlayerData, game);  // Attach game to network client
                Greenfoot.setWorld(game);
                
            } else {
                connected = false;
                messageLabel.setValueWithDelay("Connection \n Failed", delay);
                messageLabel.setValue(title);
            }
        }
    }
    
    private void updateHostIpLabel()
    {
        if(hostIP.equals(""))
            hostIpLabel.setValue("Host IP:  None");
        else
            hostIpLabel.setValue("Host IP:  " + hostIP);
    }
    
    public void act()
    {
        // if connect button clicked
        if(Greenfoot.mouseClicked(connectButton)) {
            attemptConnection();
            if(connected)
                updateHostIpLabel();
        }
                
        // If server start button clicked
        if(Greenfoot.mouseClicked(serverButton)) {
            messageLabel.setValue("Starting\nNew Server");
            client.startServer();
            messageLabel.setValue("New Server\nStarted");
            hostIP = client.getLocalIP();
            updateHostIpLabel();
        }
        
        // If set host ip button clicked
        if(Greenfoot.mouseClicked(ipButton)) {
            hostIP = Greenfoot.ask("Enter Host IP:");
            updateHostIpLabel();
        }
        
    }
}

import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.net.UnknownHostException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.net.Socket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import greenfoot.*;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;


/**
 * A class for testing network sockets.
 * 
 * @author Chan
 * @version November 2018
 */
public class GameClient
{   
    // Network connection variables
    private int port = 5444;
    private Socket socket = null;
    
    ObjectInputStream fromServer; 
    ObjectOutputStream toServer;
    
    // An instance of the game world that this client is connect to
    private GameWorld gameWorld;
    
    //private PlayerData prevPlayerInfo = new PlayerData("");  // The last known player's info
    private PlayerData curPlayerInfo;
    
    /**
     * The client that is created for each game to send and listen to server
     */
    private class Client extends Thread
    {
        public void run()
        {
            try {
                toServer = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                toServer.flush();
                fromServer = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
                
                // The remote player object info that contains all player's info
                Map<String, PlayerData> remotePlayersInfo; //= new HashMap<String, PlayerData>(); 
                while(true) {
                    if(curPlayerInfo != null && !curPlayerInfo.getName().equals("")) {
                        toServer.writeUnshared(curPlayerInfo);
                        toServer.flush();
                        remotePlayersInfo = (Map)fromServer.readObject();
                        gameWorld.update(remotePlayersInfo);
                    }
                }
            } catch(ClassNotFoundException e) {
                System.out.println(e + "error in GameClient class");
            } catch(Exception e) {
                System.out.println(e);
            }
        }
    }
        
    /**
     * Set the local player's initial network info and game instance
     */
    public void initialize(PlayerData info, GameWorld game)
    {
        // Create a copy of the current information
        this.curPlayerInfo = new PlayerData(info.getName());
        this.gameWorld = game;
    }
        
    /**
     * A method to be called inside a loop that sends information to server
     * Potential problem:  Make this client send something to server every second?
     */
    public void update(PlayerData playerInfo) throws IOException
    {
        curPlayerInfo.copyFrom(playerInfo);
    }
        
    /**
     * Returns true if this client has connected to the server
     */
    public boolean connect(String ip)
    {
        // Set up connection
        int timeout = 160;
        boolean connected = false;
        if(ip.equals("")) {
            connected = fastConnect(port, timeout);
        } else {
            connected = connectWithIp(ip);
        }
            
        if(connected) {
            try {
                new Client().start();
            } catch(Exception e) {
                System.out.println("Failed to create client thread.");
            }
            return true;
        }
        return false;
    }
    
    public boolean connectWithIp(String ip)
    {
        try {
            socket = new Socket(ip, port);
            new Client().start();
            return true;
        } catch(Exception e) {
            System.out.println("Failed to connect with ip.");
            return false;
        }
    }
    
    public void startServer()
    {
        // Start own server and connect to it
        try {
            GameServer.startServer();
            socket = new Socket(InetAddress.getLocalHost().getHostName(), port);
            System.out.println("Starting own server.");
        } catch(UnknownHostException e) {
            System.err.println("Unknown host.");
        } catch(IOException e) {
            System.err.println("Server is offline.");
        } catch(Exception e) {
            System.out.println(e);
        }
    }
    
    /**
     * Callable thread for connection search
     */
    public class Connector implements Callable<Boolean>
    {
        int start, end, timeout;
        public Connector(int start, int end, int timeout)
        {
            this.start = start;
            this.end = end;
            this.timeout = timeout;
        }
            
        @Override
        public Boolean call()
        {
            String subnet = "";
            try {
                subnet = getIP(InetAddress.getLocalHost(), true);
                Socket s = new Socket();
                for(int i = start; i < end; i++) {
                    try {
                        s = new Socket();
                        InetAddress address = InetAddress.getByName(subnet + i);
                        s.connect(new InetSocketAddress(address, port), timeout);
                        socket = s;  // Set this client's socket to the connected socket
                        return true;
                    } catch (Exception e) {
                        System.out.println(e + "Failed to Connect Socket.");
                        s.close();
                        s = null;
                    }
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }
    
    public boolean fastConnect(int port, int timeout)
    {
        int maxThreads = 12;
        int interval = 255 / maxThreads;
        List<Callable<Boolean>> tasks = new ArrayList<Callable<Boolean>>();
        for(int i = 0; i <= maxThreads; i++) {
            int start = i * interval + 1;
            int end = (i + 1) * interval;
            // ensure we don't check past 255
            end = (end > 255)? 255 : end;
            Callable<Boolean> task = new Connector(start, end, timeout);
            tasks.add(task);
        }
        ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
        boolean result = false;
        try {
            result = executor.invokeAny(tasks);
            executor.shutdown();
            if (!executor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executor.shutdownNow();
            } 
        } catch (InterruptedException e) {
            executor.shutdownNow();
        } catch(Exception e) {
            e.printStackTrace();
        }
        
        return result;
    }
    
    /**
     * Connect to host on the local computer's subnet
     */
    public boolean connectSocket(int port, int timeout)
    {   
        try{
            String subnet = getIP(InetAddress.getLocalHost(), true);  // Get the subnet of this IP
            for (int i = 1; i <= 255; i++){
                socket = new Socket();
                String hostIP = subnet + i;
                InetAddress address = InetAddress.getByName(hostIP);
                try {
                    socket.connect(new InetSocketAddress(address, port), timeout);
                    return true;
                } catch (Exception e) {
                    System.out.println(e + "Failed to Connect Socket.");
                }
                socket.close();
                socket = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }
    
    public String getLocalIP()
    {
        try {
            InetAddress addy = InetAddress.getLocalHost();
            return getIP(addy, false);
        } catch(UnknownHostException e) {
            System.out.println(e);
        }
        return "Unknown Local IP";   
    }
    
    public String getHostIP()
    {
        String ip = "";
        if(socket != null) {
            InetAddress addy = socket.getInetAddress();
            ip = getIP(addy, false);
        }
        return ip;
    }
        
    /**
     * Return the subnet of of a given InetAddress
     */
    private String getIP(InetAddress address, boolean subnetOnly)
    {
        byte[] rawBytes = address.getAddress();
        StringBuilder ipAddress = new StringBuilder();
        StringBuilder subnet = new StringBuilder();

        for(int i = 0; i < rawBytes.length - 1; i++)
        {
            ipAddress.append(rawBytes[i] & 0xFF);
            ipAddress.append(".");
        }
        
        if(!subnetOnly) {
            ipAddress.append(rawBytes[rawBytes.length-1] & 0XFF);
        }
        
        return ipAddress.toString();
    }
}
